package tv.orange.features.updater.component.data.model

enum class UpdateStatus {
    UNKNOWN,
    AVAILABLE,
    DISABLED
}